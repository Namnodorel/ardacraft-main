package me.tobi.ardacraft.main;

import me.tobi.ardacraft.main.commands.*;
import me.tobi.ardacraft.main.listener.ChatManager;
import me.tobi.ardacraft.main.listener.MainListener;

import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftMain extends JavaPlugin{
	
	private static ArdaCraftMain plugin;

	@Override
	public void onEnable() {
		plugin = this;
		this.getCommand("city").setExecutor(new CmdCity());
		this.getCommand("player").setExecutor(new CmdPlayer());
		this.getCommand("mute").setExecutor(new CmdMute());
		this.getCommand("chest").setExecutor(new CmdChest());
		this.getCommand("spawn").setExecutor(new CmdSpawn());
		this.getCommand("build").setExecutor(new CmdBuild());
		this.getCommand("list").setExecutor(new CmdList());
		this.getCommand("compass").setExecutor(new CmdCompass());//TODO City info compass, chat
		this.getCommand("mark").setExecutor(new CmdMark());
		this.getCommand("undercover").setExecutor(new CmdUndercover());
		this.getServer().getPluginManager().registerEvents(new MainListener(), this);
		this.getServer().getPluginManager().registerEvents(new ChatManager(), this);
		getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			@Override
			public void run() {
				getServer().dispatchCommand(getServer().getConsoleSender(), "co purge t:14d");
			}
		}, 2 * 60 * 20);
	}
	
	public static ArdaCraftMain getPlugin() {
		return plugin;
	}
	
}
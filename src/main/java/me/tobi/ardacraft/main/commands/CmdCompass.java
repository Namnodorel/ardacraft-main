package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.City;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Region;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class CmdCompass implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player)sender;
            if(Rank.get(p).isHigherThen(Rank.SPIELER)) {
                if(args.length > 0) {
                    CraftPlayer cplayer = ((CraftPlayer)p);
                    if(args[0].equalsIgnoreCase("city")) {
                        if(args.length == 2) {
                            City c = ArdaCraftAPI.getACDatabase().getCityManager().get(args[1]);
                            if(c != null) {
                                cplayer.setCompassTarget(c.getLocation());
                                p.sendMessage("§aZeige auf " + c.getLocation().getBlockX() + ", " + c.getLocation().getBlockY() + ", " + c.getLocation().getBlockZ());
                            }else {
                                p.sendMessage("§cBitte gibe eine gültige Stadt an!");
                            }
                        }else {
                            p.sendMessage("§cBenutzung: /compass city <Stadt>");
                        }
                    }else if(args[0].equalsIgnoreCase("location")) { //compass location x y z
                        if(args.length == 4) {
                            int x = 0;
                            int y = 0;
                            int z = 0;
                            try {
                                x = Integer.valueOf(args[1]);
                                y = Integer.valueOf(args[2]);
                                z = Integer.valueOf(args[3]);
                            }catch(NumberFormatException ex) {
                                p.sendMessage("§cBitte gib gültige X, Y und Z Werte an!");
                            }
                            cplayer.setCompassTarget(new Location(p.getWorld(), x, y, z));
                            p.sendMessage("§aZeige auf " + x + ", " + y + ", " + z);
                        }else {
                            p.sendMessage("§cBenutzung: /compass location <X> <Y> <Z>");
                        }
                    }else if(args[0].equalsIgnoreCase("region")) {
                        if(args.length == 2) {
                            Region region = ArdaCraftAPI.getACDatabase().getRegionManager().get(args[1]);
                            if(region != null) {
                                cplayer.setCompassTarget(region.getLocation());
                                p.sendMessage("§aZeige auf " + region.getLocation().getBlockX() + ", " + region.getLocation().getBlockY() + ", " + region.getLocation().getBlockZ());
                            }else {
                                p.sendMessage("§cBitte gib einen gültigen ControlPoint an!");
                            }
                        }else {
                            p.sendMessage("§cBenutzung: /compass cp <ControlPoint>");
                        }
                    }else {
                        p.sendMessage("§cBenutzung: /compass <city|cp|location>");
                    }
                }
            }
        }
        return true;
    }

}

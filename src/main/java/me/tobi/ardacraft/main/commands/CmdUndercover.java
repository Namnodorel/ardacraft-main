package me.tobi.ardacraft.main.commands;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_10_R1.*;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;
import java.util.UUID;

public class CmdUndercover implements CommandExecutor{


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player p = (Player)sender;
        p.setPlayerListName(args[0]);
        p.setDisplayName(args[0]);
        p.setCustomName(args[0]);
        //disguisePlayer(p, args[0]);

        EntityPlayer ep = ((CraftPlayer) p).getHandle();

        PacketPlayOutPlayerInfo playerInfoRemove = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ep);
        play(playerInfoRemove);
        ep.displayName = args[0];
        ep.listName = IChatBaseComponent.ChatSerializer.a(args[0]);
        PacketPlayOutPlayerInfo playerInfoAdd = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, ep);
        play(playerInfoAdd);
        //PacketPlayOutEntityDestroy destroy = new PacketPlayOutEntityDestroy();


        return true;
    }

    private void play(Packet<?> packet) {
        for (Player o : Bukkit.getOnlinePlayers()) {
            ((CraftPlayer) o).getHandle().playerConnection.sendPacket(packet);
        }
    }

    private void disguisePlayer(final Player p, String newName) {
        EntityHuman eh = ((CraftPlayer) p).getHandle();
        PacketPlayOutEntityDestroy p29 = new PacketPlayOutEntityDestroy(new int[] { p.getEntityId() });
        PacketPlayOutNamedEntitySpawn p20 = new PacketPlayOutNamedEntitySpawn(eh);
        try {
            java.lang.reflect.Field profileField = p20.getClass().getDeclaredField("b");
            profileField.setAccessible(true);
            profileField.set(p20, new GameProfile(UUID.randomUUID(), newName));
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Player o : Bukkit.getOnlinePlayers()) {
            if (!o.getName().equals(p.getName())) {
                ((CraftPlayer) o).getHandle().playerConnection.sendPacket(p29);
                ((CraftPlayer) o).getHandle().playerConnection.sendPacket(p20);
            }
        }

    }

    public static void adisguisePlayer(Player p, String newName){
        EntityHuman eh = ((CraftPlayer)p).getHandle();
        PacketPlayOutEntityDestroy p29 = new PacketPlayOutEntityDestroy(p.getEntityId());
        PacketPlayOutNamedEntitySpawn p20 = new PacketPlayOutNamedEntitySpawn(eh);
        try {
            Field profileField = p20.getClass().getDeclaredField("b");
            profileField.setAccessible(true);
            profileField.set(p20, p.getUniqueId());

        } catch (Exception e) {
            e.printStackTrace();
        }
        for(Player o : Bukkit.getOnlinePlayers()){
            if(!o.getName().equals(p.getName())){
                ((CraftPlayer)o).getHandle().playerConnection.sendPacket(p29);
                ((CraftPlayer)o).getHandle().playerConnection.sendPacket(p20);
            }
        }
    }
}

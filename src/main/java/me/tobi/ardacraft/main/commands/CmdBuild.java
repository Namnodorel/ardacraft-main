package me.tobi.ardacraft.main.commands;

import me.tobi.ardacraft.main.Builder;
import me.tobi.ardacraft.main.Statics;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CmdBuild implements CommandExecutor{
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(p.hasPermission("ardacraft.build")) {
				if(args.length == 1) {
					if(args[0].equalsIgnoreCase("menu")) {
						p.openInventory(Builder.getCategories(p));
					}
				}else {
					if(Statics.build.contains(p)) {
						Statics.build.remove(p);
					}else {
						Statics.build.add(p);
					}
				}
			}else {
				p.sendMessage("§cDu hast keine Berichtigung für diesen Befehl!");
			}
		}else {
			System.out.println("Dieser Befehl ist nur für Spieler!");
		}
		return true;
	}
	
}

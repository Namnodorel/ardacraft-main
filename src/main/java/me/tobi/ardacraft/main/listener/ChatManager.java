package me.tobi.ardacraft.main.listener;


import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.Rasse;
import me.tobi.ardacraft.api.classes.Utils;
import me.tobi.ardacraft.api.message.FancyMessage;
import me.tobi.ardacraft.main.Statics;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class ChatManager implements Listener{

    @EventHandler(priority= EventPriority.HIGHEST)
    public void onChat(AsyncPlayerChatEvent event)
    {
        String msg = event.getMessage();

        //@ nennungen
        for(Player p : Bukkit.getServer().getOnlinePlayers()) {
            Charakter c = ArdaCraftAPI.getACDatabase().getUserManager().get(p.getUniqueId().toString()).getCharakter();
            String nick = c==null?"":c.getName();
            if(nick != null) {
                if(StringUtils.containsIgnoreCase(msg, "@" + nick)) {
                    msg = msg.replace("@" + nick, "§c@" + nick + "§r");
                    PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("!"), 10, 5, 10);
                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
                }
            }
            if(StringUtils.containsIgnoreCase(msg, "@" + p.getName())) {
                msg = msg.replace("@" + p.getName(), "§c@" + p.getName() + "§r");
                PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("!"), 10, 5, 10);
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
            }
            if(StringUtils.containsIgnoreCase(msg, "@all")) {
                msg = msg.replace("@all", "§c@all§r");
                PacketPlayOutTitle title = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, IChatBaseComponent.ChatSerializer.a("§c!"), 10, 5, 10);
                ((CraftPlayer) p).getHandle().playerConnection.sendPacket(title);
            }
        }

        Player p = event.getPlayer();
        Charakter ch = Charakter.get(p);
        String dname = ch == null ? p.getName() : ch.getName();
        dname = "§6" + ChatColor.stripColor(dname) + "§f";
        PermissionUser u = PermissionsEx.getUser(p);
        String prefix = Rasse.get(p) == Rasse.UNREGISTERED ? "Neuling" : Utils.normalize(Rasse.get(p).toString());
        String suffix = u.getSuffix();
        event.setCancelled(true);
        if (Statics.muted.contains(p)) {
            p.sendMessage("§cDu wurdest gemutet und kannst momentan keine Chat-Nachrichten versenden!");
            return;
        }
        String c = Rasse.get(p).getAttitude() == Utils.Attitude.GOOD ? "§a" : "§7";
        if (Rank.get(p) == Rank.ADMIN) {
            suffix = "[§cADM§f]";
        } else if (Rank.get(p) == Rank.MOD) {
            suffix = "[§bMOD§f]";
        } else if (Rank.get(p) == Rank.CMOD) {
            suffix = "[§9CMOD§f]";
        }
        Character world = p.getWorld().getName().toCharArray()[0];
        world = Character.toUpperCase(world);

        if(msg.startsWith("#")) {
            msg = msg.replaceFirst("#", "");

            if(Rasse.get(p).getAttitude() == Utils.Attitude.GOOD) {
                msg = getColorMessage("§2", msg);
                String s = new FancyMessage("§2#GUT ").then("[§7" + world + "§r]").then("[" + c + prefix + "§f]").then(dname.replaceAll("&", "§")).tooltip(p.getName()).then(suffix.replaceAll("&", "§")).then(": " + msg).toJSONString();
                for (Player p2 : Bukkit.getServer().getOnlinePlayers()) {
                    if(Rasse.get(p2).getAttitude() == Utils.Attitude.GOOD || Rank.get(p2).isHigherThen(Rank.SPIELER)) {
                        IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
                        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
                        ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
                    }
                }
                String rasse = "#GUT [" + Rasse.get(p).toString() + "]";
                String rank = Rank.get(p).toString();
                String disname = ch == null ? p.getName() : ch.getName();
                System.out.println(rasse + p.getName() + "(" + disname + ")[" + rank + "]: " + msg);
            }else {
                msg = getColorMessage("§7", msg);
                String s = new FancyMessage("§7#BÖSE ").then("[§7" + world + "§r]").then("[" + c + prefix + "§f]").then(dname.replaceAll("&", "§")).tooltip(p.getName()).then(suffix.replaceAll("&", "§")).then(": " + msg).toJSONString();
                for (Player p2 : Bukkit.getServer().getOnlinePlayers()) {
                    if(Rasse.get(p2).getAttitude() == Utils.Attitude.BAD || Rank.get(p2).isHigherThen(Rank.SPIELER)) {
                        IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
                        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
                        ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
                    }
                }
                String rasse = "#BÖSE [" + Rasse.get(p).toString() + "]";
                String rank = Rank.get(p).toString();
                String disname = ch == null ? p.getName() : ch.getName();
                System.out.println(rasse + p.getName() + "(" + disname + ")[" + rank + "]: " + msg);
            }
        }else {
            if(msg.startsWith("+") && Rank.get(p).isHigherThen(Rank.SPIELER)) {
                msg = msg.replaceFirst("\\+", "");
                msg = getColorMessage("§c", msg);
                String s = new FancyMessage("§c#TEAM ").then("[§7" + world + "§r]").then("[" + c + prefix + "§f]").then(dname.replaceAll("&", "§")).tooltip(p.getName()).then(suffix.replaceAll("&", "§")).then(": " + msg).toJSONString();
                for (Player p2 : Bukkit.getServer().getOnlinePlayers()) {
                    if(Rank.get(p2).isHigherThen(Rank.SPIELER)) {
                        IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
                        PacketPlayOutChat packet = new PacketPlayOutChat(comp);
                        ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
                    }
                }
                String rasse = "#TEAM [" + Rasse.get(p).toString() + "]";
                String rank = Rank.get(p).toString();
                String disname = ch == null ? p.getName() : ch.getName();
                System.out.println(rasse + p.getName() + "(" + disname + ")[" + rank + "]: " + msg);
            }else {
                String s = new FancyMessage("").then("[§7" + world + "§r]").then("[" + c + prefix + "§f]").then(dname.replaceAll("&", "§")).tooltip(p.getName()).then(suffix.replaceAll("&", "§")).then(": " + msg.replaceAll("&", "§")).toJSONString();
                for (Player p2 : Bukkit.getServer().getOnlinePlayers()) {
                    IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
                    PacketPlayOutChat packet = new PacketPlayOutChat(comp);
                    ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
                }
                String rasse = "[" + Rasse.get(p).toString() + "]";
                String rank = Rank.get(p).toString();
                String disname = ch == null ? p.getName() : ch.getName();
                System.out.println(rasse + p.getName() + "(" + disname + ")[" + rank + "]: " + msg);
            }
        }
    }

    public String getColorMessage(String color, String msg) {
        String colormsg = "";
        for(char c : msg.toCharArray()) {
            colormsg += color + String.valueOf(c);
        }
        return colormsg;
    }

}

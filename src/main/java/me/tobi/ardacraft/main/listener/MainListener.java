package me.tobi.ardacraft.main.listener;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.Charakter;
import me.tobi.ardacraft.api.classes.Rank;
import me.tobi.ardacraft.api.classes.User;
import me.tobi.ardacraft.api.classes.Utils.Attitude;
import me.tobi.ardacraft.api.message.FancyMessage;
import me.tobi.ardacraft.main.ArdaCraftMain;
import me.tobi.ardacraft.main.Builder;
import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MainListener implements Listener{
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		final Player p = event.getPlayer();
		p.sendMessage("§aHerzlich willkommen auf ArdaCraft!");
		if(!ArdaCraftAPI.getACDatabase().getUserManager().contains(p.getUniqueId().toString())) {
			User u = new User(p.getUniqueId().toString(), null, p.getName(), -1, new Date().getTime());
			ArdaCraftAPI.getACDatabase().getUserManager().add(u);
            System.out.println("Added new User " + u.getLastName());
        }else {
            User u = new User(p.getUniqueId().toString(), null, p.getName(), -1, new Date().getTime());
            ArdaCraftAPI.getACDatabase().getUserManager().update(u);
        }
		String disname = Charakter.get(p)==null?p.getName():Charakter.get(p).getName();
		event.setJoinMessage("");
		String s = new FancyMessage("§8Join§a> §6").then("§6" + disname).tooltip(p.getName()).toJSONString();		
		for(Player p2 : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
			PacketPlayOutChat packet = new PacketPlayOutChat(comp);
	        ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
		}
		
		Bukkit.getServer().getScheduler().scheduleAsyncDelayedTask(ArdaCraftMain.getPlugin(), new Runnable() {
			@Override
			public void run() {

			}
		}, 20);
		if(p.getWorld().getName().equalsIgnoreCase("world")) {
			p.setGameMode(GameMode.ADVENTURE);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		final Player p = event.getPlayer();
		p.sendMessage("§cDu bist bei " + 
		(int)p.getLocation().getX() + ", " + 
		(int)p.getLocation().getY() + ", " + 
		(int)p.getLocation().getZ() + ", gestorben!" );
		final Charakter c = Charakter.get(p);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(ArdaCraftMain.getPlugin(), new Runnable() {
			@Override
			public void run() {
				try{
					if (c.getRasse().getAttitude() == Attitude.GOOD) {
						p.teleport(new Location(Bukkit.getWorld("RPG"), 5534, 69, -3525));
					} else {
						p.teleport(new Location(Bukkit.getWorld("RPG"), 1842, 91, 2524));
					}
				}catch(NullPointerException ex) {
					p.teleport(Bukkit.getWorld("world").getSpawnLocation());
				}
				p.sendMessage("§aDu wurdest zu deinem Spawn teleportiert.");
			}
		}, 20);
		

	}
	
	@EventHandler
	public void onPlayerGameModeChange(PlayerGameModeChangeEvent event) {
		if(event.getNewGameMode() == GameMode.CREATIVE) {
			if(Rank.get(event.getPlayer()) != Rank.ADMIN) {
				event.setCancelled(true);
			}
		}
	}
	
	/*@EventHandler
	public void onServerListPing(ServerListPingEvent event) {
		event.setMotd("§b§lArdaCraft §r§a- §eHerr der Ringe RPG \n§c§l>> Jetzt Joinen <<");		
	}*/
	
	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		Player p = event.getPlayer();
		String disname = Charakter.get(p)==null?p.getName():Charakter.get(p).getName();
		event.setQuitMessage("");
		String s = new FancyMessage("§8Left§c> §6").then("§6" + disname).tooltip(p.getName()).toJSONString();		
		for(Player p2 : Bukkit.getServer().getOnlinePlayers()){
			IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(s);
			PacketPlayOutChat packet = new PacketPlayOutChat(comp);
	        ((CraftPlayer) p2).getHandle().playerConnection.sendPacket(packet);
		}

		User u = new User(p.getUniqueId().toString(), null, p.getName(), -1, new Date().getTime());
		ArdaCraftAPI.getACDatabase().getUserManager().update(u);
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if(event.getInventory().getHolder() instanceof Player) {
			Inventory inv = event.getInventory();
			Player p = (Player) inv.getHolder();
			if(event.getCurrentItem() != null) {
				ItemStack item = event.getCurrentItem();
				if(p.hasPermission("ardacraft.build")) {
					event.setCancelled(Builder.handleInventoryClick(inv, item, p));
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if(event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			World world = p.getWorld();
			if(world.getName().equalsIgnoreCase("world")) {
				event.setCancelled(true);
				return;
			}
		}
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			World world = p.getWorld();
			if(world.getName().equalsIgnoreCase("world")) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if(event.getEntity() instanceof Player) {
			Player p = (Player) event.getEntity();
			World world = p.getWorld();
			if(world.getName().equalsIgnoreCase("world")) {
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		World world = p.getWorld();
		if(world.getName().equalsIgnoreCase("world")) {
			if(!Rank.get(p).isHigherThen(Rank.MOD)) {
				event.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event) {
		event.blockList().clear();
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		Material type = event.getBlock().getType();
		World world = event.getBlock().getLocation().getWorld();
		if(world.getName().equalsIgnoreCase("RPG")) {
			List<Material> ores = new ArrayList<>();
			ores.add(Material.DIAMOND_ORE);
			ores.add(Material.COAL_ORE);
			ores.add(Material.REDSTONE_ORE);
			ores.add(Material.EMERALD_ORE);
			ores.add(Material.GOLD_ORE);
			ores.add(Material.IRON_ORE);
			ores.add(Material.LAPIS_ORE);
			if(ores.contains(type)) {
				event.setCancelled(true);
				event.getBlock().setType(Material.AIR);
				event.getPlayer().sendMessage("§cErze bringen in dieser Welt keinen Ertrag. Zum Erze abbauen gehe bitte in die Mining Welt!");
			}
		}
	}
	
	@EventHandler
	public void onBlockPlayceEvent(BlockPlaceEvent event) {
		Player p = event.getPlayer();
		Material type = event.getBlock().getType();
		World world = p.getWorld();
		if(world.getName().equalsIgnoreCase("RPG")) {
			List<Material> ores = new ArrayList<>();
			ores.add(Material.DIAMOND_ORE);
			ores.add(Material.COAL_ORE);
			ores.add(Material.REDSTONE_ORE);
			ores.add(Material.EMERALD_ORE);
			ores.add(Material.GOLD_ORE);
			ores.add(Material.IRON_ORE);
			ores.add(Material.LAPIS_ORE);
			if(ores.contains(type)) {
				if(!p.isSneaking()) {
					event.setCancelled(true);
					p.sendMessage("§cAchtung! In dieser Welt platzierte Erze droppen beim Abbauen keine Items!");
					p.sendMessage("§cDu kannst in dieser Welt nur sneakend (schleichend) Erze platzieren!");
				}else {
					p.sendMessage("§cAchtung! In dieser Welt platzierte Erze droppen beim Abbauen keine Items!");
				}
			}
		}
	}
	
}